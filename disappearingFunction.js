function funcBuilder(){
    var counter = 0;
    function innerFunc(){
        counter++;
        if(counter < 4){
            console.log(counter);
            return true;
        }
    }
    return innerFunc;
}

var disappearingFunction = funcBuilder(); // this is our closure. It has its environment closed over it, so it has its own instance of the counter variable.
var disappearingFunction2 = funcBuilder(); // this is another closure, which will have a SEPARATE INSTANCE of the counter variable.


disappearingFunction();
disappearingFunction();
disappearingFunction();
disappearingFunction();
disappearingFunction();
disappearingFunction();
disappearingFunction();

// disappearingFunction2();
// disappearingFunction2();
// disappearingFunction2();
// disappearingFunction2();
// disappearingFunction2();
// disappearingFunction2();
// disappearingFunction2();




