function batSignal(){
    var _batCaveGear = {
        batmobile:"vroom!",
        batsuit:"looks awesome",
        batarangs:"ouch!",
        tagline:"I'm batman"
    };
    function batman(){
        console.log('It\'s the signal! Commence inventory check!');
        // iterate through stuff that is NOT a part of the Batman function, but part of the environment it came from.
        for(var key in _batCaveGear){
            console.log(key+': '+_batCaveGear[key]);
        }
        console.log("ready to dispense justice!");
    }
    return batman; // batSignal returns Batman, but doesn't activate Batman by using (). Batman takes all the gear in the Batcave with him!
}

var callBatman = batSignal(); // Because batSignal returns the batman function (NOT the return value of that function) callBatman is now a function. This is the closure.

callBatman(); // we now activate the closure, and we still have access to it's scope chain via JavaScript magic. This is why we can access batCaveGear, an object in the local scope, from out here in the global scope. Effectively we've created a private variable. Batman to the rescue!