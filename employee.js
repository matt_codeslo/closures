// Here we create a single environment shared by THREE functions. They are all closures, but since they are created at the same time, they share their environment with each other. If you use setName to alter _data, showInfo will reflect it. Awesome!

// Note, this isn't necessarily the best way to create this type of functionality. Come to our presentation on protoypes in coming months.

var Employee = function(firstName,lastName,department){
    var _data ={
        firstName:firstName,
        lastName:lastName,
        department:department
    };

    return {
        setName: function(fn,ln){
            _data.firstName = fn;
            _data.lastName = ln;
        },
        setDepartment: function(dep){
            _data.department = dep;
        },
        showInfo: function(){
            for(var key in _data){
                console.log(key + ':'+_data[key]);
            }
        }
    };
};

var matt = Employee('Matt','West','Applications');

var rich = Employee('Rich','J','Applications');

matt.showInfo();

rich.showInfo();

console.log(matt._data);


