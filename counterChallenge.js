function countMe(num){
    var counter = 0;
    function innerFunc(){
        counter+=num;
        return counter;
    }
    return innerFunc;
}

var counterChallenge = countMe(5); // this is the closure

console.log(counterChallenge());
console.log(counterChallenge());
console.log(counterChallenge());
console.log(counterChallenge());
console.log(counterChallenge());
console.log(counterChallenge());
